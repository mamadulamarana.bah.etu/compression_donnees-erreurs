PARTIE 1

Q1.3 :
a partir de 0.02

q1.4 :
A partir de 0.004
Q1.5
0.0001

q1.6:
plus un fichier est compressé, plus il est sensible aux modifications de bits
une image est plus sensible aux erreurs qu'un fichier zip.

et Plus le seuil de probabilité est élévée plus il est compliqué de reussir à corriger toutes les erreurs d'un fichier.
C'est pourquoi il est preferable d'avoir un seuil faible pour corriger un max d'erreur.

PARTIE 2

q2.1 :
(bytes[1] & bytes[2]) | (bytes[0] & bytes[1]) | (bytes[0] & bytes[2]);  


q2.4 :
la sortie de chaque octet est bien triplé => BBBooonnnjjjooouuurrr   tttooouuuttt   llleee   mmmooonnndddeee;

q2.5
echo "Bonjour tout le monde" | ./encode repeat3 | ./decode repeat3

q2.7 :
cat TP-Erreurs.zip | ./encode repeat3 | ./cbssm 0.01 | ./decode repeat3 > archive.zip
le seuil de proba est 0.01, l'archive ne peut plus être décompressé au dessus de ce seuil

q2.8 :
cat data/lille.gif | ./encode repeat3 | ./cbssm 0.001 | ./decode repeat3 > lille.gif
le seuil de proba est 0.001, l'image ne peut plus etre lu au dessus de ce seuil.

PARTIE 3
q3.2 et q3.3
le resultat du codage de (word) est correct car il a poids de hamming pair, ce qui signifie que le bit de parité a bien été ajouté.

PARTIE 4
Selon le seuil de probabilité,nous remarquons que avec les methodes hammingp,hamming et parity2d il y a moins d'octets qui different apres correction donc il y un grand nombre d'erreurs qui ont été corrigées contrairement aux methodes repeat3 et repeat3-linear où la difference en octet est encore grande.
Ainsi on retient que les codages les plus efficaces pour corriger n'importe quel type de fichier sont les codages parity2d,hamming et hammingp.

